//
//  ViewController.m
//  HelloWorld
//
//  Created by Eric Garbade on 1/23/17.
//  Copyright © 2017 Eric Garbade. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController
@synthesize helloLabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)clicked:(id)sender {
    [helloLabel setText:@"Hello, User!"];
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"galaxy.jpg"]]];
}

@end
