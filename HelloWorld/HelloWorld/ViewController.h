//
//  ViewController.h
//  HelloWorld
//
//  Created by Eric Garbade on 1/23/17.
//  Copyright © 2017 Eric Garbade. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *helloLabel;
@property (strong, nonatomic) IBOutlet UIButton *button;


-(IBAction)clicked:(id)sender;

@end

